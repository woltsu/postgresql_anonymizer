## PRO TIP : Test your modifications locally with:
## $ gitlab-ci-multi-runner exec docker {name_of_the_job}


variables:
    PGDATA: /var/lib/postgresql/data
    PGUSER: postgres
    EXTDIR: /usr/share/postgresql/11/extension/
    PSQL: psql -v ON_ERROR_STOP=1
    POSTGRES_DB: nice_marmot
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: plop

stages:
  - lint
  - build
  - test
  - deploy

image: postgres:11


##
## We run most tests on the postgres instance inside the docker image.
## We need to launch manually the instance because the entrypoint is skipped.
##
before_script:
    - mkdir -p $PGDATA
    - mkdir -p $EXTDIR
    - chown postgres $PGDATA
    - gosu postgres initdb
    - gosu postgres pg_ctl start


##
## L I N T
##

lint-bash:
  stage: lint
  before_script:
    - echo "disable the before_script"
  script:
    - apt-get update && apt-get install -y shellcheck
    - shellcheck bin/standalone.sh
    - shellcheck bin/pg_dump_anon.sh
    - shellcheck docker/anon.sh

lint-markdown:
  stage: lint
  image: ruby:alpine
  before_script:
    - echo "disable the before_script"
  script:
    - gem install mdl
    - mdl docs/*.md *.md

##
## B U I L D
##

# PostgreSQL 9.5 is not supported
make9.5:
  stage: build
  image: postgres:9.5
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-9.5 pgxnclient
    - pgxn install ddlx
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day
  when: manual
  allow_failure: yes

make9.6:
  stage: build
  image: postgres:9.6
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-9.6 pgxnclient
    - pgxn install ddlx
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day

make10:
  stage: build
  image: postgres:10
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-10 pgxnclient
    - pgxn install ddlx
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - export PG_TEST_EXTRA=''
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day

make11:
  stage: build
  image: postgres:11
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-11 pgxnclient
    - pgxn install ddlx
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day

make12:
  stage: build
  image: postgres:12
  script:
    - apt-get update && apt-get install -y make gcc git postgresql-server-dev-12 pgxnclient
    - pgxn install ddlx
#    - git clone https://github.com/lacanoid/pgddl.git
#    - make -C pgddl && make -C pgddl install
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day

make13:
  stage: build
  image: postgres:13
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-13
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make installcheck || diff results tests/expected
  artifacts:
    paths:
        - anon*
        - regression.*
        - results/
    expire_in: 1 day

make11-centos:
  stage: build
  image: centos:7
  before_script:
    - echo 'ignoring global before_script'
  script:
    - yum -y install https://download.postgresql.org/pub/repos/yum/11/redhat/rhel-7-x86_64/pgdg-centos11-11-2.noarch.rpm
    - yum -y install git make gcc postgresql11 postgresql11-server postgresql11-devel pgxnclient python-setuptools
    - /usr/pgsql-11/bin/pg_config
    - export PATH="$PATH:/usr/pgsql-11/bin"
    - pgxn install ddlx
    - cd /tmp
    - git clone https://gitlab.com/dalibo/postgresql_anonymizer.git anonymizer
    - cd anonymizer
    - make
    - make install
  when: manual

standalone12:
  stage: build
  services:
    - postgres:12
  before_script:
    - echo "disable the before_script"
  script:
    - apt-get update && apt-get install -y make git postgresql-server-dev-all
    - make anon_standalone.sql
    - export PGPASSWORD=$POSTGRES_PASSWORD
    - $PSQL -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f anon_standalone.sql
    - $PSQL -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c 'SELECT anon.fake_city() IS NOT NULL'

standalone11:
  stage: build
  services:
    - postgres:11
  before_script:
    - echo "disable the before_script"
  script:
    - apt-get update && apt-get install -y make git postgresql-server-dev-all
    - make anon_standalone.sql
    - export PGPASSWORD=$POSTGRES_PASSWORD
    - $PSQL -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f anon_standalone.sql
    - $PSQL -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c 'SELECT anon.fake_city() IS NOT NULL'

blackbox:
  stage: build
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  before_script:
    - echo 'Disable before_script.'
  script:
    - docker build -t blackbox . --file docker/Dockerfile
    - mkdir results
    - cat tests/sql/blackbox.sql | docker run --rm -i blackbox /anon.sh  > results/blackbox.out
    # we remove comments because pg_dump ouputs the PG version and we don't want
    # this test to break every time a new minor version is released
    - sed -i 's/^--.*$//' results/blackbox.out
    - diff tests/expected/blackbox.out results/blackbox.out

##
## T E S T
##

demo:
  stage: test
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-11 postgresql-contrib-11  pgxnclient
    - pgxn install ddlx
    - make extension
    - make install
    - psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon'"
    - psql -c "SELECT pg_reload_conf();"
    - make demo
  when: manual
  artifacts:
    paths:
        - demo/*.out
    expire_in: 1 day


##
## D E P L O Y
##

docker_latest:
  stage: deploy
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  before_script:
    - echo 'Disable before_script.'
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/dalibo/postgresql_anonymizer:latest . --file docker/Dockerfile
    - docker push $CI_REGISTRY/dalibo/postgresql_anonymizer:latest
  only:
    - master@dalibo/postgresql_anonymizer

docker_stable:
  stage: deploy
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  before_script:
    - echo 'Disable before_script.'
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/dalibo/postgresql_anonymizer:stable . --file docker/Dockerfile
    - docker push $CI_REGISTRY/dalibo/postgresql_anonymizer:stable
  only:
    - stable@dalibo/postgresql_anonymizer


pgxn:
  stage: deploy
  image: alpine
  before_script:
    - echo 'Disable before_script.'
  script:
    - apk update && apk add make git zip
    - make pgxn
  artifacts:
    paths:
      - anon*
      - _pgxn/
    expire_in: 1 day
  only:
    - master

test_pgxn:
  stage: deploy
  before_script:
    - echo "disable the before_script"
  script:
    - apt-get update && apt-get install -y make gcc postgresql-server-dev-11 pgxnclient
    - pgxn install postgresql_anonymizer
  when: manual

# test install
test_pgxn_ubuntu_pg95:
  stage: deploy
  image: ubuntu:bionic
  before_script:
    - echo "disable the before_script"
  script:
    - apt-get update && apt-get install -y make gcc postgresql-common pgxnclient gnupg ca-cacert
    - yes '' | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh bionic
    - apt-get install -y postgres-9.5 postgres-9.5-contrib postgresql-server-dev-9.5
    - export PGDATA=/var/lib/postgresql/data
    - export PGUSER=postgres
    - su postgres -c /usr/lib/postgresql/9.5/bin/initdb
    - su postgres -c "/usr/lib/postgresql/9.5/bin/pg_ctl start" &
    - pg_config --sharedir
    - pgxn install --pg_config /usr/lib/postgresql/9.5/bin/pg_config ddlx
    - pgxn install --pg_config /usr/lib/postgresql/9.5/bin/pg_config postgresql_anonymizer
    - psql -c "CREATE EXTENSION ddlx;"
    - psql -c "CREATE EXTENSION tsm_system_rows;"
    - psql -c "CREATE TEMPORARY TABLE pg_config AS SELECT 'SHAREDIR'::TEXT AS name, '/usr/share/postgresql/9.5'::TEXT AS setting;CREATE EXTENSION anon;"
    - psql -c "ALTER DATABASE postgres SET session_preload_libraries='anon'"
    - psql -c "SELECT anon.load();"
  when: manual

test_yum_centos7_pg12:
  stage: deploy
  image: centos:7
  before_script:
    - echo 'ignoring global before_script'
  script:
    - yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    - yum -y install postgresql12-contrib postgresql_anonymizer12
    - mkdir -p $PGDATA
    - chown postgres $PGDATA
    - su postgres -c /usr/pgsql-12/bin/initdb $PGDATA
    - su postgres -c "/usr/pgsql-12/bin/pg_ctl start"
    - su postgres -c "psql -c 'CREATE EXTENSION anon CASCADE;'"
  when: manual
